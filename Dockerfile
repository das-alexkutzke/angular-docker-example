FROM node:16-alpine3.12 as env

RUN apk add chromium

ENV CHROME_BIN=/usr/bin/chromium-browser

FROM env as build
COPY . src
WORKDIR src

RUN   npm install && \
      npm run build

FROM build as test
RUN  npm run test

FROM nginx AS release
COPY --from=build src/dist/calculator/ /usr/share/nginx/html/

